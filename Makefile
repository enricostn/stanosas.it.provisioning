project = stanosas
container = $(project)-dev
define LXC_CONF
# Network configuration
lxc.network.type = veth
lxc.network.link = lxcbr0
lxc.network.flags = up

# Shared directories
lxc.mount.entry = /home/enrico/dev/$(project)/$(project).it /var/lib/lxc/$(container)/rootfs/home/ubuntu/$(project) none bind,create=dir 0.0
endef
lxc_conf_path = /tmp/lxc_stanosas.conf

submodules:
	git submodule init
	git submodule update

dependencies:
	ansible-galaxy install -r dependencies.yml --roles-path=vendor

setup:
	@echo 'Generating LXC configuration at $(lxc_conf_path)'
	$(file > $(lxc_conf_path),$(LXC_CONF))

	sudo lxc-create -n $(container) -f $(lxc_conf_path) -t ubuntu -- --release xenial --arch amd64
	sudo lxc-start  -n $(container)
	sudo lxc-wait   -n $(container) -s RUNNING
	sleep 10 # Waiting for ip address
	sudo sed -i '/local.$(project).it/d' /etc/hosts
	echo `sudo lxc-info -n $(container) | grep IP | awk '{ print $$2 }'` local.$(project).it | sudo tee -a /etc/hosts
	ssh-keygen -f ~/.ssh/known_hosts -R local.$(project).it
	ssh-copy-id -i ~/.ssh/id_rsa.pub ubuntu@local.$(project).it
	# sudo lxc-attach -n $(container) -- apt -y update
	# sudo lxc-attach -n $(container) -- apt -y install openssh-server python2.7
	# sudo lxc-attach -n $(container) -- usermod -l $(project) -m -d /home/$(project) ubuntu
	# sudo lxc-attach -n $(container) -- groupmod -n $(project) ubuntu
	# sudo lxc-attach -n $(container) -- passwd $(project)

provision:
	ansible-playbook -i hosts playbook.yml -l development --ask-sudo-pass -e ansible_user=ubuntu -e 'ansible_python_interpreter=/usr/bin/python2.7'

i_feel_lucky: submodules setup dev
